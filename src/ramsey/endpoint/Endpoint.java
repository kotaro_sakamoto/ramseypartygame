package ramsey.endpoint;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author K.Sakamoto
 */
public class Endpoint {
    private static final Map<Integer, Integer> endpoints = Collections.unmodifiableMap(new HashMap<Integer, Integer>(){{
        put(5, 0); put(7, 3); put(8, 0); put(9, 0);
    }});

    public static Map<Integer, Integer> getEndpoints() {
        return endpoints;
    }
}

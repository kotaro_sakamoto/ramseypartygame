package ramsey;

import java.util.*;

/**
 * @author K.Sakamoto
 */
public class State {
    private int layer;
    private Map<Edge, Boolean> state;

    public State() {
        initialize();
        setLayer(0);
    }

    public State(Edge... edges) {
        this();
        for (Edge edge : edges) {
            set(edge);
        }
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public int getLayer() {
        return layer;
    }

    public void set(Edge edge) {
        ++layer;
        state.put(edge, true);
    }

    public void unset(Edge edge) {
        --layer;
        state.put(edge, false);
    }

    public boolean notFull() {
        return !isFull();
    }

    public boolean isFull() {
        return layer == Edges.getAllEdgesLength();
    }

    public boolean get(Edge edge) {
        return state.get(edge);
    }

    public Boolean[] get() {
        List<Boolean> list = new ArrayList<>(Edges.getAllEdgesLength());
        for (Edge edge : Edges.getAllEdges()) {
            list.add(get(edge));
        }
        return list.toArray(new Boolean[list.size()]);
    }

    private void initialize() {
        state = new HashMap<>();
        for (Edge edge : Edges.getAllEdges()) {
            unset(edge);
        }
    }

    public List<Edge> getEdges() {
        List<Edge> list = new ArrayList<>(Edges.getAllEdgesLength());
        for (Edge edge : Edges.getAllEdges()) {
            if (state.get(edge)) {
                list.add(edge);
            }
        }
        return list;
    }

    public List<Edge> getEmptyEdges() {
        List<Edge> list = new ArrayList<>(Edges.getAllEdgesLength());
        for (Edge edge : Edges.getAllEdges()) {
            if (!state.get(edge)) {
                list.add(edge);
            }
        }
        return list;
    }

    public void drawEdge(int node1, int node2) {
        Edge edge = Edges.draw(node1, node2);
        if ((edge != Edge.NONE) && !this.get(edge)) {
            this.set(edge);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Edge edge : Edges.getAllEdges()) {
            if (get(edge)) {
                builder.append(Edges.toString(edge));
                builder.append(System.lineSeparator());
            }
        }
        return builder.toString();
    }
}

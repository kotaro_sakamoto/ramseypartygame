package ramsey;

import java.util.ArrayList;
import java.util.List;

/**
 * @author K.Sakamoto
 */
public class Triangle {
    private static final State[] triangles = {
            new State() {{set(Edge.E12); set(Edge.E13); set(Edge.E23);}},
            new State() {{set(Edge.E12); set(Edge.E14); set(Edge.E24);}},
            new State() {{set(Edge.E12); set(Edge.E15); set(Edge.E25);}},
            new State() {{set(Edge.E12); set(Edge.E16); set(Edge.E26);}},
            new State() {{set(Edge.E13); set(Edge.E14); set(Edge.E34);}},
            new State() {{set(Edge.E13); set(Edge.E15); set(Edge.E35);}},
            new State() {{set(Edge.E13); set(Edge.E16); set(Edge.E36);}},
            new State() {{set(Edge.E14); set(Edge.E15); set(Edge.E45);}},
            new State() {{set(Edge.E14); set(Edge.E16); set(Edge.E46);}},
            new State() {{set(Edge.E15); set(Edge.E16); set(Edge.E56);}},

            new State() {{set(Edge.E23); set(Edge.E24); set(Edge.E34);}},
            new State() {{set(Edge.E23); set(Edge.E25); set(Edge.E35);}},
            new State() {{set(Edge.E23); set(Edge.E26); set(Edge.E36);}},
            new State() {{set(Edge.E24); set(Edge.E25); set(Edge.E45);}},
            new State() {{set(Edge.E24); set(Edge.E26); set(Edge.E46);}},
            new State() {{set(Edge.E25); set(Edge.E26); set(Edge.E56);}},

            new State() {{set(Edge.E34); set(Edge.E35); set(Edge.E45);}},
            new State() {{set(Edge.E34); set(Edge.E36); set(Edge.E46);}},
            new State() {{set(Edge.E35); set(Edge.E36); set(Edge.E56);}},

            new State() {{set(Edge.E45); set(Edge.E46); set(Edge.E56);}}
    };

    private static boolean hasTriangle(State state1, State state2) {
        Boolean[] nodes1 = state1.get();
        Boolean[] nodes2 = state2.get();
        int counter = 0;
        for (int i = 0; i < nodes1.length; ++i) {
            if (nodes1[i] && nodes2[i]) {
                ++counter;
            }
        }
        return 3 <= counter;
    }

    public static boolean hasTriangle(State state) {
        for (State triangle : triangles) {
            if (hasTriangle(state, triangle)) {
                return true;
            }
        }
        return false;
    }

    public static boolean doesNotHaveTriangle(State state) {
        return !Triangle.hasTriangle(state);
    }

    public static State[] getTriangles(State state) {
        List<State> list = new ArrayList<>();
        for (State triangle : triangles) {
            if (hasTriangle(state, triangle)) {
                list.add(triangle);
            }
        }
        return list.toArray(new State[list.size()]);
    }
}

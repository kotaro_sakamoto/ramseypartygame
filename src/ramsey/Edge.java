package ramsey;

/**
 * @author K.Sakamoto
 */
public enum Edge {
    E12,
    E13,
    E14,
    E15,
    E16,
    E23,
    E24,
    E25,
    E26,
    E34,
    E35,
    E36,
    E45,
    E46,
    E56,
    NONE;
}

package ramsey;

import java.util.List;

/**
 * @author K.Sakamoto
 */
public class Player {
    public Player() {
    }

    public boolean isWinner(State state, Player enemy) {
        if (state.isFull()) {
            return false;
        }
       //System.out.println(state.getLayer());

        List<Edge> emptyEdges = state.getEmptyEdges();

        for (int i = 0; i < emptyEdges.size(); ++i) {
            Edge edge = emptyEdges.get(i);

            state.set(edge);

            //loose case
            if (Triangle.hasTriangle(state) || enemy.isWinner(state, this)) {
                emptyEdges.remove(edge);
            }

            state.unset(edge);
        }
        return !emptyEdges.isEmpty();

    }
}

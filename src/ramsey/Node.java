package ramsey;

/**
 * @author K.Sakamoto
 */
public class Node {
    private int[] node;

    public Node() {
        node = new int[7];
        for (int n : node) {
            n = 0;
        }
    }

    public int[] getNode() {
        return node;
    }

    public int getCount(int index) {
        return node[index];
    }

    public void setCount(int index, int count) {
        node[index] = count;
    }

    public void count(int index) {
        ++node[index];
    }
}

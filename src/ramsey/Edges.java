package ramsey;

/**
 * @author K.Sakamoto
 */
public class Edges {
    private static final Edge[] allEdges = {
            Edge.E12,
            Edge.E13,
            Edge.E14,
            Edge.E15,
            Edge.E16,
            Edge.E23,
            Edge.E24,
            Edge.E25,
            Edge.E26,
            Edge.E34,
            Edge.E35,
            Edge.E36,
            Edge.E45,
            Edge.E46,
            Edge.E56
    };
    private static final int allEdgesLength = allEdges.length;

    public static Edge[] getAllEdges() {
        return allEdges;
    }

    public static int getAllEdgesLength() {
        return allEdgesLength;
    }

    public static String toString(Edge edge) {
        switch (edge) {
            case E12: return "E12";
            case E13: return "E13";
            case E14: return "E14";
            case E15: return "E15";
            case E16: return "E16";
            case E23: return "E23";
            case E24: return "E24";
            case E25: return "E25";
            case E26: return "E26";
            case E34: return "E34";
            case E35: return "E35";
            case E36: return "E36";
            case E45: return "E45";
            case E46: return "E46";
            case E56: return "E56";
            default: return "NONE";
        }
    }

    public static Edge draw(int node1, int node2) {
        int n1;
        int n2;
        if (node1 > node2) {
            n1 = node2;
            n2 = node1;
        } else {
            n1 = node1;
            n2 = node2;
        }
        switch (n1) {
            case 1:
                switch (n2) {
                    case 2: return Edge.E12;
                    case 3: return Edge.E13;
                    case 4: return Edge.E14;
                    case 5: return Edge.E15;
                    case 6: return Edge.E16;
                }
            case 2:
                switch (n2) {
                    case 3: return Edge.E23;
                    case 4: return Edge.E24;
                    case 5: return Edge.E25;
                    case 6: return Edge.E26;
                }
            case 3:
                switch (n2) {
                    case 4: return Edge.E34;
                    case 5: return Edge.E35;
                    case 6: return Edge.E36;
                }
            case 4:
                switch (n2) {
                    case 5: return Edge.E45;
                    case 6: return Edge.E46;
                }
            case 5:
                switch (n2) {
                    case 6: return Edge.E56;
                }
        }
        return Edge.NONE;
    }
}

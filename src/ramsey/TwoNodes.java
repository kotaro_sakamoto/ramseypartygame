package ramsey;

/**
 * @author K.Sakamoto
 */
public class TwoNodes {
    private int numOfEdgesWithRightNode;
    private int numOfEdgesWithLeftNode;

    public TwoNodes(int numOfEdgesWithLeftNode, int numOfEdgesWithRightNode) {
        int left;
        int right;
        if (numOfEdgesWithLeftNode > numOfEdgesWithRightNode) {
            left = numOfEdgesWithRightNode;
            right = numOfEdgesWithLeftNode;
        } else {
            left = numOfEdgesWithLeftNode;
            right = numOfEdgesWithRightNode;
        }
        setNumOfEdgesWithLeftNode(left);
        setNumOfEdgesWithRightNode(right);
    }

    private void setNumOfEdgesWithRightNode(int numOfEdgesWithRightNode) {
        this.numOfEdgesWithRightNode = numOfEdgesWithRightNode;
    }

    private void setNumOfEdgesWithLeftNode(int numOfEdgesWithLeftNode) {
        this.numOfEdgesWithLeftNode = numOfEdgesWithLeftNode;
    }

    public int getNumOfEdgesWithRightNode() {
        return numOfEdgesWithRightNode;
    }

    public int getNumOfEdgesWithLeftNode() {
        return numOfEdgesWithLeftNode;
    }

    public boolean equals(TwoNodes twoNodes) {
        return (this.numOfEdgesWithLeftNode == twoNodes.getNumOfEdgesWithLeftNode()) &&
                (this.numOfEdgesWithRightNode == twoNodes.getNumOfEdgesWithRightNode());
    }

}

package ramsey;

import java.util.ArrayList;
import java.util.List;

/**
 * @author K.Sakamoto
 */
public class Nodes {
    private static int[] getArray(int node1, int node2) {
        return new int[] {node1, node2};
    }

    public static int[] getNodes(Edge edge) {
        switch (edge) {
            case E12: return Nodes.getArray(1, 2);
            case E13: return Nodes.getArray(1, 3);
            case E14: return Nodes.getArray(1, 4);
            case E15: return Nodes.getArray(1, 5);
            case E16: return Nodes.getArray(1, 6);
            case E23: return Nodes.getArray(2, 3);
            case E24: return Nodes.getArray(2, 4);
            case E25: return Nodes.getArray(2, 5);
            case E26: return Nodes.getArray(2, 6);
            case E34: return Nodes.getArray(3, 4);
            case E35: return Nodes.getArray(3, 5);
            case E36: return Nodes.getArray(3, 6);
            case E45: return Nodes.getArray(4, 5);
            case E46: return Nodes.getArray(4, 6);
            case E56: return Nodes.getArray(5, 6);
            default: return Nodes.getArray(0, 0);
        }
    }

    public static List<Integer> getEndpointNodes(State state) {
        int[] nodes = new int[6];
        for (int node : nodes) {
            node = 0;
        }

        for (Edge edge : state.getEdges()) {
            for (int node : Nodes.getNodes(edge)) {
                ++nodes[--node];
            }
        }

        List<Integer> list = new ArrayList<>(6);

        for (int node : nodes) {
            if (node == 1) {
                list.add(node);
            }
        }

        return list;
    }

    public static Node convertNode(State state) {
        Node node = new Node();
        for (Edge edge : state.getEdges()) {
            for (int n : Nodes.getNodes(edge)) {
                node.count(n);
            }
        }
        return node;
    }

    public static TwoNodes convertTwoNodes(Edge edge, Node node) {
        int[] nodes = Nodes.getNodes(edge);
        return new TwoNodes(node.getCount(nodes[0]), node.getCount(nodes[1]));
    }
}

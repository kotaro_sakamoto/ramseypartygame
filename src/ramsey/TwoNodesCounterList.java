package ramsey;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author K.Sakamoto
 */
public class TwoNodesCounterList implements Iterable<TwoNodesCounter> {
    private State state;
    private List<TwoNodesCounter> twoNodesCounters;

    private void initialize() {
        twoNodesCounters = new ArrayList<>();
    }

    private TwoNodesCounterList(State state) {
        setState(state);
        initialize();
    }

    private void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public static TwoNodesCounterList create(State state) {
        return new TwoNodesCounterList(state).addAll(state);
    }

    public TwoNodesCounterList add(TwoNodes twoNodes) {
        for (TwoNodesCounter twoNodesCounter : this) {
            if (twoNodesCounter.equals(twoNodes)) {
                twoNodesCounter.count();
                return this;
            }
        }
        twoNodesCounters.add(new TwoNodesCounter(twoNodes));
        return this;
    }

    public TwoNodesCounterList addAll(State state) {
        Node node = Nodes.convertNode(state);
        List<Edge> edges = state.getEdges();
        for (Edge edge : edges) {
            this.add(Nodes.convertTwoNodes(edge, node));
        }
        return this;
    }

    public int size() {
        return twoNodesCounters.size();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (TwoNodesCounter tnc : this) {
            builder.append(String.format("(%d, %d) * %d%n",
                    tnc.getNumOfEdgesWithLeftNode(),
                    tnc.getNumOfEdgesWithRightNode(),
                    tnc.getCounter() + 1));
        }
        builder.append(getState().toString());
        builder.append(System.lineSeparator());
        builder.append("---");
        builder.append(System.lineSeparator());
        return builder.toString();
    }

    private boolean hasTwoNodes(TwoNodes twoNodes) {
        for (TwoNodesCounter twoNodesCounter : twoNodesCounters) {
            if (twoNodes.equals(twoNodesCounter)) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(TwoNodesCounterList twoNodesCounterList) {
        if (size() == twoNodesCounterList.size()) {
            int counter = 0;
            for (TwoNodesCounter twoNodesCounter : twoNodesCounterList) {
                if (hasTwoNodes(twoNodesCounter)) {
                    ++counter;
                }
            }
            return (counter == size());
        }
        return false;
    }

    @Override
    public Iterator<TwoNodesCounter> iterator() {
        return twoNodesCounters.iterator();
    }
}

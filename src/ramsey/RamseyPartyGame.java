package ramsey;

import ramsey.layer.*;

/**
 * @author K.Sakamoto
 */
public class RamseyPartyGame {
    private Player firstPlayer;
    private Player secondPlayer;

    public RamseyPartyGame() {
        firstPlayer = new Player();
        secondPlayer = new Player();
    }

    public void startFromNthLayer(int layer) {
        for (State initState : Layer.getStates(layer)) {
            System.out.println("---");
            System.out.println(initState.toString());
            start(initState);
        }
    }

    public void start(State initState) {
        System.out.println("Start");
        System.out.printf("The winner is The %s Player!%n%n", firstPlayer.
            isWinner(initState, secondPlayer) ? "First" : "Second");
    }

    public static void main(String[] args) {
        //RamseyPartyGame rpg = new RamseyPartyGame();
        //rpg.start(new State());
        //rpg.startFromNthLayer(4);

        //NthBasis basis = new NthBasis(5);
        //System.out.println(basis.toString());

        Link link = new Link();
        //link.printAllLinks();
        link.printEndpoints();
    }
}

package ramsey;

/**
 * @author K.Sakamoto
 */
public class TwoNodesCounter extends TwoNodes {
    private int counter;

    public TwoNodesCounter(TwoNodes twoNodes) {
        super(twoNodes.getNumOfEdgesWithLeftNode(), twoNodes.getNumOfEdgesWithRightNode());
        counter = 0;
    }

    public void count() {
        ++counter;
    }

    public int getCounter() {
        return counter;
    }

    public boolean equals(TwoNodesCounter twoNodesCounter) {
        return super.equals(twoNodesCounter) &&
                (twoNodesCounter.getCounter() == counter);
    }
}

package ramsey.layer;

import ramsey.Edge;
import ramsey.State;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author K.Sakamoto
 */
public class Layer {
    private static final List<List<State>> statesList = Collections.unmodifiableList(Arrays.asList(
            //First Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(1, 1) * 1
                    new State(Edge.E12)
            )),
            //Second Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(1, 2) * 2
                    new State(Edge.E12, Edge.E13),
                    //(1, 1) * 2
                    new State(Edge.E12, Edge.E34)
            )),
            //Third Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(1, 3) * 3
                    new State(Edge.E12, Edge.E13, Edge.E14),
                    //(2, 2) * 1
                    //(1, 2) * 2
                    new State(Edge.E12, Edge.E13, Edge.E24),
                    //(1, 2) * 2
                    //(1, 1) * 1
                    new State(Edge.E12, Edge.E13, Edge.E45),
                    //(1, 1) * 3
                    new State(Edge.E12, Edge.E34, Edge.E56)
            )),
            //Fourth Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(1, 4) * 4
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E15),
                    //(2, 3) * 1
                    //(1, 3) * 2
                    //(1, 2) * 1
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E25),
                    //(1, 3) * 3
                    //(1, 1) * 1
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E56),
                    //(2, 2) * 4
                    new State(Edge.E12, Edge.E14, Edge.E23, Edge.E34),
                    //(2, 2) * 2
                    //(1, 2) * 2
                    new State(Edge.E12, Edge.E14, Edge.E23, Edge.E35),
                    //(2, 2) * 1
                    //(1, 2) * 2
                    //(1, 1) * 1
                    new State(Edge.E12, Edge.E14, Edge.E23, Edge.E56),
                    //(1, 2) * 4
                    new State(Edge.E12, Edge.E23, Edge.E45, Edge.E46)
            )),
            //Fifth Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(1, 5) * 5
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E15, Edge.E16),
                    //(2, 4) * 1
                    //(1, 4) * 3
                    //(1, 2) * 1
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E15, Edge.E26),
                    //(3, 3) * 1
                    //(1, 3) * 4
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E25, Edge.E26),
                    //(2, 3) * 2
                    //(1, 3) * 1
                    //(1, 2) * 2
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E26, Edge.E35),
                    //(1, 3) * 1
                    //(2, 3) * 2
                    //(2, 2) * 2
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E35, Edge.E45),
                    //(1, 3) * 2
                    //(2, 3) * 1
                    //(2, 2) * 1
                    //(1, 2) * 1
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E45, Edge.E56),
                    //(1, 2) * 2
                    //(2, 2) * 3
                    new State(Edge.E12, Edge.E23, Edge.E34, Edge.E45, Edge.E56),
                    //(1, 1) * 1
                    //(2, 2) * 4
                    new State(Edge.E12, Edge.E34, Edge.E36, Edge.E45, Edge.E56),
                    //(2, 2) * 5
                    new State(Edge.E13, Edge.E14, Edge.E23, Edge.E25, Edge.E45)
            )),
            //Sixth Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(2, 4) * 2
                    //(1, 4) * 2
                    //(2, 2) * 2
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E15, Edge.E26, Edge.E36),
                    //(1, 3) * 2
                    //(3, 3) * 1
                    //(2, 3) * 2
                    //(2, 2) * 1
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E35, Edge.E36, Edge.E45),
                    //(1, 3) * 1
                    //(2, 3) * 2
                    //(2, 2) * 3
                    new State(Edge.E12, Edge.E13, Edge.E15, Edge.E36, Edge.E45, Edge.E46),
                    //(2, 3) * 6
                    new State(Edge.E12, Edge.E14, Edge.E15, Edge.E23, Edge.E34, Edge.E35),
                    //(2, 3) * 4
                    //(1, 3) * 2
                    new State(Edge.E12, Edge.E14, Edge.E15, Edge.E23, Edge.E35, Edge.E36),
                    //(2, 3) * 3
                    //(1, 2) * 1
                    //(2, 2) * 2
                    new State(Edge.E12, Edge.E14, Edge.E15, Edge.E23, Edge.E46, Edge.E56),
                    //(2, 2) * 6
                    new State(Edge.E13, Edge.E14, Edge.E23, Edge.E25, Edge.E46, Edge.E56)
            )),
            //Seventh Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(2, 4) * 3
                    //(1, 4) * 1
                    //(2, 3) * 3
                    new State(Edge.E12, Edge.E13, Edge.E14, Edge.E15, Edge.E26, Edge.E36, Edge.E46),
                    //(3, 3) * 2
                    //(2, 3) * 4
                    //(1, 3) * 1
                    new State(Edge.E12, Edge.E15, Edge.E16, Edge.E23, Edge.E24, Edge.E35, Edge.E36),
                    //(3, 3) * 1
                    //(2, 3) * 4
                    //(2, 2) * 2
                    new State(Edge.E12, Edge.E15, Edge.E16, Edge.E23, Edge.E24, Edge.E36, Edge.E45),
                    //(2, 3) * 6
                    //(2, 2) * 1
                    new State(Edge.E13, Edge.E14, Edge.E15, Edge.E23, Edge.E24, Edge.E26, Edge.E56)
            )),
            //Eighth Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(2, 4) * 8
                    new State(Edge.E12, Edge.E14, Edge.E15, Edge.E16, Edge.E23, Edge.E34, Edge.E35, Edge.E36),
                    //(3, 3) * 4
                    //(2, 3) * 4
                    new State(Edge.E12, Edge.E15, Edge.E16, Edge.E23, Edge.E24, Edge.E35, Edge.E36, Edge.E45)
            )),
            //Ninth Layer
            Collections.unmodifiableList(Arrays.asList(
                    //(3, 3) * 9
                    new State(Edge.E13, Edge.E15, Edge.E16, Edge.E23, Edge.E25, Edge.E26, Edge.E34, Edge.E45, Edge.E46)
            ))
    ));
    public static List<List<State>> getStatesList() {
        return statesList;
    }

    public static List<State> getStates(int layer) {
        return ((0 < layer) && (layer <= statesList.size())) ?
                statesList.get(--layer) : new ArrayList<State>();
    }
}

package ramsey.layer;


import ramsey.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author K.Sakamoto
 */
public class Link {
    public Link() {

    }

    public void printAllLinks() {
        for (int i = 1; i < 9; ++i) {
            printLinks(i);
        }
    }

    public void printLinks(int lowerLayer) {
        int upperLayer = lowerLayer + 1;
        List<List<Integer>> linksList = getLinks(lowerLayer);
        System.out.printf("%d from %d%n", upperLayer, lowerLayer);
        for (int i = 0; i < linksList.size(); ++i) {
            List<Integer> links = linksList.get(i);
            System.out.printf("%s is connected to...%n", i);//upperStates.get(i).toString());
            for (Integer link : links) {
                System.out.println(link);//lowerStates.get(link).toString());
            }
            System.out.println("---".concat(System.lineSeparator()));
        }
    }

    public void printEndpoints() {
        List<List<Integer>> list = getEndpoints();
        for (int i = 0; i < list.size(); ++i) {
            List<Integer> endpoints = list.get(i);
            if (!endpoints.isEmpty()) {
                int layer = i + 1;
                System.out.printf("---%nLayer %d%n", layer);
                List<State> states = Layer.getStates(layer);
                for (Integer endpoint : endpoints) {
                    System.out.printf("%d%n", endpoint);
                    System.out.println(states.get(endpoint).toString());
                }
            }
        }
    }

    private List<Integer> getEndpoints(int lowerLayer) {
        List<Integer> list = new ArrayList<>();
        for (List<Integer> links : getLinks(lowerLayer)) {
            for (Integer link : links) {
                if (!list.contains(link)) {
                    list.add(link);
                }
            }
        }
        return getExclusiveList(list, Layer.getStates(lowerLayer).size());
    }

    private List<Integer> getExclusiveList(List<Integer> list, int max) {
        List<Integer> exclusiveList = new ArrayList<>(max);
        for (int i = 0; i < max; ++i) {
            if (!list.contains(i)) {
                exclusiveList.add(i);
            }
        }
        return exclusiveList;
    }

    private List<List<Integer>> getEndpoints() {
        List<List<Integer>> list = new ArrayList<>(8);
        for (int i = 1; i < 10; ++i) {
            list.add((i == 9) ? getIntList(9) : getEndpoints(i));
        }
        return list;
    }

    private List<Integer> getIntList(int layer) {
        int max = Layer.getStates(layer).size();
        List<Integer> list = new ArrayList<>(max);
        for (int i = 0; i < max; ++i) {
            list.add(i);
        }
        return list;
    }

    private List<List<Integer>> getLinks(int lowerLayer) {
        List<List<Integer>> result = new ArrayList<>();
        int upperLayer = lowerLayer + 1;
        for (State state : Layer.getStates(upperLayer)) {
            List<Integer> list = new ArrayList<>();
            List<Integer> links = getLinks(state, lowerLayer);
            for (Integer link : links) {
                if (!list.contains(link)) {
                    list.add(link);
                }
            }
            result.add(list);
        }
        return result;
    }

    private List<Integer> getLinks(State state1, int lowerLayer) {
        List<Integer> list = new ArrayList<>();
        List<TwoNodesCounterList> tncls = new ArrayList<>();
        for (State state : Layer.getStates(lowerLayer)) {
            tncls.add(TwoNodesCounterList.create(state));
        }
        for (Edge edge : state1.getEdges()) {
            state1.unset(edge);
            if (Triangle.doesNotHaveTriangle(state1)) {
                TwoNodesCounterList tncl1 = TwoNodesCounterList.create(state1);
                for (int i = 0; i < tncls.size(); ++i) {
                    TwoNodesCounterList tncl2 = tncls.get(i);
                    if (tncl1.equals(tncl2) && !list.contains(i)) {
                        list.add(i);
                    }
                }
            }
            state1.set(edge);
        }
        return list;
    }
}

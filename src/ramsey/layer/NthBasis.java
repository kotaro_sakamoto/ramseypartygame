package ramsey.layer;

import ramsey.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author K.Sakamoto
 */
public class NthBasis implements Iterable<TwoNodesCounterList> {
    List<TwoNodesCounterList> twoNodesCountersList;
    int max;
    int[][] nodes;
    int layer;

    public NthBasis(int layer) {
        setLayer(layer);
        setMax(Edges.getAllEdgesLength());
        initTwoNodesCountersList();
        initNodes();

        start();
    }

    private void start() {
        addAllStates(new State(), 0);
    }

    private void initTwoNodesCountersList() {
        twoNodesCountersList = new ArrayList<>();
    }

    private void initNodes() {
        Edge[] edges = Edges.getAllEdges();
        int length = edges.length;
        nodes = new int[length][];
        for (int i = 0; i < length; ++i) {
            nodes[i] = Nodes.getNodes(edges[i]);
        }
    }

    private void setMax(int max) {
        this.max = max;
    }

    private void setLayer(int layer) {
        this.layer = layer;
    }

    private void addTwoNodesCounters(TwoNodesCounterList twoNodesCounters) {
        System.out.println(twoNodesCounters.getState().toString());
        twoNodesCountersList.add(twoNodesCounters);
    }

    private boolean contains(TwoNodesCounterList twoNodesCounters) {
        for (TwoNodesCounterList tncl : this) {
            if (twoNodesCounters.equals(tncl)) {
                return true;
            }
        }
        return false;
    }

    private boolean notContains(TwoNodesCounterList twoNodesCounters) {
        return !contains(twoNodesCounters);
    }

    public void addAllStates(State state, int min) {
        if (state.getLayer() != layer) {
            for (int i = min; i < max; ++i) {
                int[] node = nodes[i];
                Edge edge = Edges.draw(node[0], node[1]);
                state.set(edge);
                if (Triangle.doesNotHaveTriangle(state)) {
                    addAllStates(state, ++min);
                }
                state.unset(edge);
            }
        } else {
            if ((state.getEdges().size() == layer) && Triangle.doesNotHaveTriangle(state)) {
                TwoNodesCounterList tncl = TwoNodesCounterList.create(state);
                if (notContains(tncl)) {
                    addTwoNodesCounters(tncl);
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(twoNodesCountersList.size());
        for (TwoNodesCounterList tncl : this) {
            builder.append(tncl.toString());
        }
        builder.append(twoNodesCountersList.size());
        builder.append(System.lineSeparator());
        return builder.toString();
    }

    @Override
    public Iterator<TwoNodesCounterList> iterator() {
        return twoNodesCountersList.iterator();
    }
}
